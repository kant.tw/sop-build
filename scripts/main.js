'use strict';

document.addEventListener('DOMContentLoaded', function () {

	// Toggles

	var $toggler = $('.js-toggle-trigger');

	$toggler.each(function ($el) {
		var _this = this;

		$(this).on('click', function (e) {
			e.preventDefault();
			var target = $(_this).data('target');
			var $target = $('#' + target);
			$(_this).toggleClass('active');
			$target.toggleClass('active');
		});
	});

	// header menu - mobile
	var $mobileMenu = $('.header_menu');
	if ($mobileMenu.length > 0) {
		$mobileMenu.on('click', function (e) {
			e.preventDefault();
			if (!$('html, body').hasClass('mmenu-opened')) {
				// to open the menu
				// alert('to open' + $('body').scrollTop());
				$('html').data('prevscroll', $('html').scrollTop());
				$('body').data('prevscroll', $('body').scrollTop());
				$('body').addClass('mmenu-opened');
				$('.offcanvas ul li').eq(0).find('a').focus();
			} else {
				// to close the menu
				// alert('to close' + $('body').data('prevscroll'));
				$('html, body').removeClass('mmenu-opened');
				$('html').scrollTop($('html').data('prevscroll'));
				$('body').scrollTop($('body').data('prevscroll'));
			}
		});
	}

	// close mobile menu
	$('.pageCover').on('click', function () {
		$('.header_menu').click();
	});

	// footer accordion when mobile
	if ($(window).width() < 1280) {
		$('.js-footer-toggle').on('click', footerToggle);
		$('body').bind('click', closeMemberNav);
	}
	$(window).resize(function () {
		if ($(window).width() < 769) {
			$('body').bind('click', closeMemberNav);
		} else {
			$('body').unbind('click', closeMemberNav);
		}
	});

	// home carousel
	$('.js-carousel').slick({
		slidesToShow: 2,
		slidesToScroll: 1,
		// centerMode: false,
		arrows: true,
		responsive: [{
			breakpoint: 2000,
			settings: {
				slidesToShow: 3,
				arrows: true,
				centerMode: true,
				centerPadding: '15%'
			}
		}, {
			breakpoint: 1009,
			settings: {
				slidesToShow: 2,
				arrows: false,
				centerMode: false
			}
		}, {
			breakpoint: 768,
			settings: {
				slidesToShow: 2,
				centerMode: false,
				// centerPadding: '17%',
				arrows: true
			}
		}]
	});

	// magnific popup
	$('.js-mfp-inline').each(function (index, ele) {
		$(ele).magnificPopup({
			type: 'inline'
		});
	});
	$('.js-mfp-close').on('click', function (e) {
		e.preventDefault();
		$.magnificPopup.close();
	});

	$('.js-login').magnificPopup();

	$('.backTop').on('click', function (e) {
		e.preventDefault();
		$('html, body').animate({ 'scrollTop': 0 }, 500);
	});

	function footerToggle(event) {
		event.preventDefault();
		$(event.target).toggleClass('active');
		$(event.target).next('.submenu').toggleClass('active');
	}

	function closeMemberNav(event) {
		if ($(event.target).parents('.memberNav').length === 0) {
			if ($('.memberNav_toggle').hasClass('active')) {
				$('.memberNav_toggle').click();
			}
		}
	}
});

function Tab(div) {
	var _this2 = this;

	//div : .tabwrap or #myTab
	this.activeIdx = 0;
	this.tabLists = $(div + ' .tab-list').find('li').length > 0 ? $(div + ' .tab-list li') : $(div + ' .tab-list a');
	this.tabContents = $(div + ' .tab-cnt');

	//init
	this.init = function () {
		_this2.bindClick();
		_this2.tabLists.eq(_this2.activeIdx).click();
	};

	this.bindClick = function () {
		var self = _this2;
		self.tabLists.on('click', function (e) {
			e.preventDefault();
			self.activeIdx = $(e.target).index();
			self.tabLists.removeClass('active');
			$(e.target).addClass('active');
			self.tabContents.removeClass('active');
			self.tabContents.eq(self.activeIdx).addClass('active');
		});
	};
}