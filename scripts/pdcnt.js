function pdContentUI(options) {
    
    this.mainWrapClass = options.mainPicWrap;
    this.colorChanger = $(options.colorChanger);
    this.sizeSelector = $(options.sizeSelector);
    this.numberChanger = $(options.numberChanger);
    this.currentColor = $(options.initColor);
    this.mainSlide = options.mainSlideClass;
    this.thumb = options.thumbClass;
    this.currentSize;
    this.currentNumber;
    this.currentQty; 
    this.colornameElem = $(options.colornameElem);

    
    this.init = function() {
        var self = this;

        this.colorEvent();
        this.sizeChangeEvent();
        this.qtyChangEvent();
        
        this.currentSize = this.sizeSelector.eq(0).find('select').val();
        this.currentQty = this.sizeSelector.eq(0).find('option:selected').data('qty');
    }
        
}

//pdcontent點顏色 換左邊整組圖片&換尺寸數量
pdContentUI.prototype.colorEvent = function() {
    var self = this;
    this.colorChanger.on('click', function(e) {
        e.preventDefault();
        var colorValue = $(this).data('color');
        self.currentColor = colorValue; 

        //改變選顏色區樣式
        self.colorChanger.parent('li').removeClass('active');
        self.colornameElem.text($(this).data('colorname'));
        $(this).parent('li').addClass('active');

        //換圖
        self.goToTargetSlide($(this).data('slideidx'));

        //換尺寸數量
        self.activeTargetSize(colorValue);

    });
}

pdContentUI.prototype.goToTargetSlide = function(colorSlideIndex) {
    // console.log("test", colorSlideIndex);
    $(this.mainSlide).slick("slickGoTo", colorSlideIndex);
}

pdContentUI.prototype.activeTargetSize = function(colorVal) {
    var self = this;
    self.sizeSelector.removeClass('active');
    self.sizeSelector.each(function() {
        if ($(this).data('color') === colorVal) {
            $(this).addClass('active');

            if ($(this).find('option:selected').data('qty')) {
                self.currentQty = $(this).find('option:selected').data('qty');    
            } else { //current selected size does not have qty
                self.currentQty = $(this).find('option').eq(1).data('qty');
            }
            self.buildNumberSelect(self.currentQty);
        }
    });
    //更新值狀態
    self.currentNumber = 1;
    self.fillHiddenInput(self.currentColor, self.currentSize, self.currentNumber);
}

//切換尺寸
pdContentUI.prototype.sizeChangeEvent = function() {
    var self = this;
    self.sizeSelector.find('select').on('change', function(e) {
        self.currentSize = $(e.target).val();
        
        //重新建構數量下拉
        self.buildNumberSelect(parseInt($(e.target).find('option:selected').data('qty')));
        if (parseInt($(e.target).find('option:selected').data('qty')) === 0) {
            $('.pdcnt_info_btn').addClass('empty');
            self.numberChanger.attr('disabled', true);
        } else {
            $('.pdcnt_info_btn').removeClass('empty');
            self.numberChanger.attr('disabled', false);
            self.currentNumber = 1;
        }
        self.fillHiddenInput(self.currentColor, self.currentSize, self.currentNumber);
    });
}

pdContentUI.prototype.buildNumberSelect = function(qty) {
    $('.pdcnt_info_size').show();
    var optionHtml = '<option>QTY</option>';
    this.numberChanger.empty();
    if (qty) {
        for (var i = 0; i < qty; i++) {
            optionHtml = optionHtml + '<option value="' + (i+1) + '">' + (i+1) + '</option>';
        }    
    }
    this.numberChanger.append(optionHtml);
    this.numberChanger.show();
}

pdContentUI.prototype.qtyChangEvent = function() {
    var self = this;
    self.numberChanger.on('change', function() {
        var num = parseInt(self.numberChanger.val());
        self.currentNumber = num;
        self.fillHiddenInput(self.currentColor, self.currentSize, self.currentNumber)
    });
}

pdContentUI.prototype.fillHiddenInput = function(color, size, pdnumber) { 
    $('input[name="pdcolor"]').val(color); 
    $('input[name="pdsize"]').val(size); 
    $('input[name="pdnumber"]').val(pdnumber); 
    // console.log('color: ' + $('input[name="pdcolor"]').val() + ',size: ' + $('input[name="pdsize"]').val() + ',number: ' + $('input[name="pdnumber"]').val());
}